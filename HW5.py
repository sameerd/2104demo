"""
@date: 2/22/2019
@author: Sameer Dandekar
@PID: sameerd
@assignment: HW5
@note: Do NOT alter the function headers that are well documented
"""


def max_difference(values: list) -> float:
    """ Efficiently finds the largest difference between any two elements in a list
    @param values: a list of numbers
    @return number for the largest difference between elements
    """
    
    # check which of the two first values is greater and calculate initial max difference
    if (values[0] > values[1]):
        max_diff = values[0] - values[1];
        min = values[1];
        max = values[0];
    else:
        max_diff = values[1] - values[0];
        min = values[0];
        max = values[1];
        
    # find maximum and minimum values in the array    
    for i in range(len(values)):
        if (values[i] > max):
            max = values[i];
        
        if (values[i] < min):
            min = values[i];
                
    # return max - min as the max difference if greater than the initial max difference            
    if (max - min > max_diff):
        return max - min;
    else:
        return max_diff;

    


def sort_bivalued(values: list) -> list:
    """Efficiently sort a list of binary values
    @param values: a list of binary digits (0 or 1)
    @return: a list of binary numbers in ascending sort order
    """
    
    j = -1;
    
    
    for i in range(len(values)):
        
        # check if a number is 0
        if (values[i] < 1):
            j = j + 1;
            
            # swap 0s
            temp = values[i];
            values[i] = values[j];
            values[j] = temp; 
            
        
    
    return values;